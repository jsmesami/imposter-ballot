default: deploy

deploy:
	aws s3 sync public/ s3://imposter.cz --delete
	aws cloudfront create-invalidation --distribution-id E2I92HOS3CRGOK --paths "/*"
